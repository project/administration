/*
permission to GPL granted by original author Kae Verens - kae@verens.com
note -- this is a modified version of the original
*/

if (isJsEnabled()) {
  addLoadEvent(collapse_div);
  if (document.getElementById)
    window.onunload=div_save_state
}

function collapse_div(){
 cdId=0;
 if(!document.getElementsByTagName)return; // reject non-compliant browsers
 a=document.getElementsByTagName('DIV');
 for(i=0;a[i];i++){
   if (!hasClass(a[i], 'collapsible')) continue;
   b=a[i].childNodes;
   for(j=0;b[j];j++){
     if (hasClass(b[j], 'head')) {
       c=document.createElement('a');
       c.setAttribute('href','javascript:cdSwitch("cd'+(cdId)+'")');
       c.setAttribute('id','cd'+(cdId)+'A');
       c.setAttribute('class','opener');
       c.style.display='inline';
       c.innerHTML='&nbsp;&nbsp;[-]';
       b[j].appendChild(c);
     } 
     if (hasClass(b[j], 'body')) {
       b[j].setAttribute('id','cd'+(cdId++));
       b[j].style.display='block';
     }
   }
 }
 var cookie_value=div_get_cookie("collapsible_div");
 if (cookie_value.length > 0) {
   var ids = cookie_value.split('|');
   for (var j=0; j<ids.length-1; j++) {
     cdSwitch(ids[j]);
   }
 }
}

function cdSwitch(id){
 a=document.getElementById(id);
 a.style.display=(a.style.display=='block')?'none':'block';
 if(parentLinkIsOpener)return;
 a=document.getElementById(id+'A');
 a.innerHTML=(a.innerHTML=='&nbsp;&nbsp;[-]')?'&nbsp;&nbsp;[+]':'&nbsp;&nbsp;[-]';
}

function div_save_state() {
  var cookie_name = "collapsible_div", cookie_value = "", div_id = "";
  i=0;
  while (document.getElementById("cd"+i)){
    a=document.getElementById("cd"+i);
    b=a.getAttribute("style");
    if (typeof b == 'string') {
      c=b;
    }
    else if (typeof b == 'object') {
      c=b.cssText;
    } 
    if (c=="display: none;" | c=="DISPLAY: none"){
      cookie_value += a.getAttribute("id")+"|"; 
    }
    i++;
    parent.document.cookie=cookie_name+"="+cookie_value+";expires=Fri, 31 Dec 2099 23:59:59 GMT;";
  }
}

function div_get_cookie(name) {
  var search_value = name + "="
  var return_value = "";
  if (document.cookie.length > 0) {
    begin = document.cookie.indexOf(search_value)
    if (begin != -1) {
      begin += search_value.length
      end = document.cookie.indexOf(";", begin);
      if (end == -1) end = document.cookie.length;
      return_value=unescape(document.cookie.substring(begin, end))
    }
  }
  return return_value;
}
