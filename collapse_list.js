/*
permission to GPL granted by original author Kae Verens - kae@verens.com
note -- this is a modified version of the original
*/
var parentLinkIsOpener=0; 

if (isJsEnabled()) {
  addLoadEvent(collapse_list);
}

function collapse_list(){
 cmId=0;
 if(!document.getElementsByTagName)return; // reject non-compliant browsers
 a=document.getElementsByTagName('UL');
 for(i=0;a[i];i++){
  if(a[i].getElementsByTagName('UL')){
   // a[i] is an object which has a collapsible list in it
   if (!hasClass(a[i], 'collapsible')) {
     continue;
   }
   b=a[i].childNodes;
   for(j=0;b[j];j++){
    if(b[j].nodeName=='LI'){
     d=b[j].getElementsByTagName('ul');
     if(d.length){
      c=document.createElement('a');
      c.setAttribute('href','javascript:cmSwitch("cm'+(cmId)+'")');
      c.setAttribute('id','cm'+(cmId)+'A');
      if(parentLinkIsOpener){
       // we've chosen to use the parent link as the opener
       var e=b[j].innerHTML;
       //e=e.replace(/\n/g,'');
       e=e.replace(/(\n|\r)/g,'');
       e=e.replace(/(<ul|<UL).*/,'');
       e=e.replace(/<[^>]*>/g,'');
       c.innerHTML=e;
       b[j].replaceChild(c,b[j].childNodes[0]);
      }else{
       // create a new [+] link as the opener
       c.style.display='inline';
       c.innerHTML='&nbsp;&nbsp;[+]';
       b[j].insertBefore(c,b[j].lastChild);
      }
      d[0].setAttribute('id','cm'+(cmId++));
      d[0].style.display='none';
     }
    }
   }
  }
 }
}

function cmSwitch(id){
 a=document.getElementById(id);
 a.style.display=(a.style.display=='block')?'none':'block';
 if(parentLinkIsOpener)return;
 a=document.getElementById(id+'A');
 a.innerHTML=(a.innerHTML=='&nbsp;&nbsp;[+]')?'&nbsp;&nbsp;[-]':'&nbsp;&nbsp;[+]';
}
