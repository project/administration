<?php

/*
 * The administration menu config has three basic pieces. 'section' which
 * are separate because they need their own menu entries, 'menu'
 * which need to be individually reconfigured, and 'alias' items in
 * case we want to alias menu items around.
 *
 * Each section entry has:
 * 'title' => The short title
 * 'description' => The longer descriptive title
 * 'callback' => The callback to use for this section. If blank no menu entry
 *               will be provided.
 * 
 * Each menu entry has
 *  'title' => The short title of the menu entry
 *  'description' => The longer desc
 *  'weight' => A weight can be specified; this will override the default weight 
 *              and is not recommended; the default weight will put menu items
 *              in the order they appear in the array.
 */
function _administration_menu_config() {
  $config['cssfile'] = 'civicspace.css';
  $config['default']['sub_panel_theme'] = 'cs_administration_sub_menu_panel';

  // This is for new links so that they get the right permissions.
  // Kind of annoying honestly.

  $config['section']['sadmin/features'] = array(
    'title'       => t('features'),
    'description' => t('New Features and Applications'),
    'long-desc'   => t("You can configure new applications, known as modules, and learn how to add new features."),
    'callback'    => '_administration_sub_dashboard_page',
    'panel_theme' => 'cs_administration_menu_panel',
    'sub_panel_theme' => 'cs_administration_sub_menu_panel',
    'sub_panel_css' => 'dashboard',
    'items' => array(
      'admin/help' => array(
        'title' => 'help',
        'description' => 'Application (Module) help',
      ),
      'sadmin/features/support' => array(
        'title' => 'get support',
        'description' => 'Get support',
        'short-desc' => 'A page that explains support options, including paid support',
        'long-desc' => 'This exists as an example of what the long-desc will do',
        'callback' => 'administration_offsite',
        'callback arguments' => array('http://drupal.org/support'),
      ),
      'admin/modules' => array(
        'title' => 'activate/deactivate',
        'description' => 'Turn on and off applications (modules)',
      ),
       'admin/settings' => array(
        'title' => 'configure modules',
        'description' => "Configure my site's module settings",
      ),
      'admin' => array(
        'title' => 'other',
        'description' => "Configure uncategorized items for modules",
      ),
      'chat' => array(
        'title' => 'chat',
        'description' => 'Chat live to volunteers',
        'short-desc' => 'Well researched questions may be answered',
        'callback' => 'administration_offsite',
        'callback arguments' => array('http://drupal.org/support'),
      ),
    ),
  );

  $config['section']['sadmin/content'] = array(
    'title'       => t('content'),
    'description' => t('Manage web pages (content)'),
    'long-desc'   => t('Web pages are treated as content that can be managed with settings'),
    'callback'    => '_administration_sub_dashboard_page',
    'panel_theme' => 'cs_administration_menu_panel',
    'sub_panel_theme' => 'cs_administration_sub_menu_panel',
    'sub_panel_css' => 'dashboard',
    'items' => array(
      'admin/comment' => array(
        'title' => 'manage comments',
        'description' => "Moderate and publish comments",
      ),
      'admin/taxonomy' => array(
        'title' => 'manage categories',
        'description' => "Set up and maintain your site's categories (vocabularies/taxonomy)",
      ),
      'node/add' => array(
        'title' => 'create content',
        'description' => "Add new web pages (content) to your site",
      ),
      'sadmin/content/no-link1' => array(
        'title' => 'apply tags',
        'description' => 'Apply tags to web pages (content)',
        'callback' => 'drupal_not_found',
      ),
      'admin/node' => array(
        'title' => 'organize',
        'description' => 'Order and organize web pages (content)',
      ),
    ),
  );
    
  $config['section']['sadmin/build'] = array(
    'title'       => t('build'),
    'description' => t('Build your web site'),
    'long-desc'   => t('Adminstering users, installing new applications, upgrading, creating menus, checking versions'),
    'callback'    => '_administration_sub_dashboard_page',
    'panel_theme' => 'cs_administration_menu_panel',
    'sub_panel_theme' => 'cs_administration_sub_menu_panel',
    'sub_panel_css' => 'dashboard',
    'items' => array(
      'sadmin/build/no-link2' => array(
        'title' => 'install modules',
        'description' => 'Install new modules',
        'short-desc' => 'Download module files, database install',
        'callback' => 'drupal_not_found'
      ),
      'admin/menu' => array(
        'title' => 'menus',
        'description' => "Create and configure menus",
      ),
      'admin/access' => array(
        'title' => 'access control',
        'description' => "Setup site security (access control to site features)",
      ),
      'admin/path' => array(
        'title' => 'url aliases',
        'description' => "Create and manage URL aliases",
      ),
      'admin/filters' => array(
        'title' => 'input formats',
        'description' => "Configure allowable input formats/input filters",
      ),
    ),
  );

  $config['section']['sadmin/style'] = array(
    'title'       => t('style'),
    'description' => t('Style and layout'),
    'long-desc'   => t('Administering style and layout elements of your web site'),
    'callback'    => '_administration_sub_dashboard_page',
    'panel_theme' => 'cs_administration_menu_panel',
    'sub_panel_theme' => 'cs_administration_sub_menu_panel',
    'sub_panel_css' => 'dashboard',
    'items' => array(
      'admin/themes' => array(
        'title' => 'themes',
        'description' => "Activate/configure themes",
      ),
      'admin/block' => array(
        'title' => 'blocks',
        'description' => "Activate/configure blocks",
      ),
      'sadmin/style/no-link1' => array(
        'title' => 'fix themes',
        'description' => 'Fix theme problems',
        'callback' => 'drupal_not_found',
      ),
      'sadmin/style/no-link2' => array(
        'title' => 'page layout',
        'description' => 'Page layout',
        'short-desc' => 'Page layouts, front pages, number of columns',
        'callback' => 'drupal_not_found'
      ),
    ),
  );

  $config['section']['sadmin/overview'] = array(
    'title'       => t('site overview'),
    'description' => t('Site overview'),
    'long-desc'   => t('View site users, manage content, view system logs'),
    'callback'    => '_administration_sub_dashboard_page',
    'panel_theme' => 'cs_administration_menu_panel',
    'sub_panel_theme' => 'cs_administration_sub_menu_panel',
    'sub_panel_css' => 'dashboard',
    'items' => array(
      'admin/user' => array(
        'title' => 'manage users',
        'description' => "Change or add users",
      ),
      'admin/node' => array(
        'title' => 'manage content',
        'description' => "Moderate, promote and manage your web pages",
      ),
      'admin/logs' => array(
        'title' => 'logs',
        'description' => "Site errors and other logs",
      ),
    ),
  );

  $config['section']['sadmin/mail'] = array(
    'title'       => t('mail'),
    'description' => t('Mail and Subscriptions'),
    'long-desc'   => t('Manage mail, compose newsletters, subscription management'),
    'callback'    => '_administration_sub_dashboard_page',
    'panel_theme' => 'cs_administration_menu_panel',
    'sub_panel_theme' => 'cs_administration_sub_menu_panel',
    'sub_panel_css' => 'dashboard',
    'items' => array(
      'sadmin/mail/no-link1' => array(
        'title' => 'mailing lists',
        'description' => 'Mailing lists',
        'callback' => 'drupal_not_found',
      ),
      'admin/aggregator' => array(
        'title' => 'news feeds',
        'description' => "Manage my news feeds",
      ),
    ),
  );

  $config['section']['sadmin/statistics'] = array(
    'title'       => t('statistics'),
    'description' => t('Site Statistics'),
    'long-desc'   => t('A glance at administrative statistics about your site'),
    'callback'    => '_administration_sub_dashboard_page',
    'panel_theme' => 'administration_statistics',
    'sub_panel_css' => 'dashboard',
  );
  
  // This special one clones the system settings section so it goes into the
  // right place
  $config['section']['sadmin/build/settings'] = array(
    'title'       => t('site settings'),
    'description' => t('Configure site-wide settings'),
    'callback'    => 'system_site_settings',
  );

  // overrides to repoint a couple of settings.
  $config['section']['admin/settings'] = array(
    'title' => t('configure modules'),
    'description' => t("Configure my site's module settings"),
    'callback'    => '_administration_settings_page',
  );
  
  // overrides to repoint a couple of settings.
  $config['section']['admin'] = array(
    'title' => t('configure modules'),
    'description' => t("Configure uncategorized module settings"),
    'callback'    => '_administration_sub_dashboard_page',
  );

  $config['dashboard']['dashboard'] = array(
    'sadmin/features', 
    'sadmin/content',
    'sadmin/build',
    'sadmin/style',
    'sadmin/overview',
    'sadmin/mail',
    'sadmin/statistics',
  );

  return $config;
}

function theme_cs_administration_menu_panel($path) {
  // Just a copy of the standard one, but uses 2 columns.

  $config = _administration_menu_config();
  $menu = menu_get_menu();
  $mid = $menu['path index'][$path];
  $output .= '<div class=dashboard-panel>';
  $output .= '<div class=dashboard-panel-head>';
  $output .= $menu['items'][$mid]['description'];
  $output .= '</div>';
  $output .= '<div class=dashboard-panel-body>';
  $output .= '<div class=dashboard-panel-desc>';
  $output .= $config['section'][$path]['long-desc']; 
  $output .= '</div>';
  $col1 = '<div id="col1"><ul>';
  $col2 = '<div id="col2"><ul>';
  $counter = 0;
  foreach ($menu['visible'][$mid]['children'] as $cid) {
    $item = '';
    $item .= '<li>';
    $title = !empty($menu['items'][$cid]['description']) ? $menu['items'][$cid]['description'] : $menu['items'][$cid]['title'];
    $item .= l($title, $menu['items'][$cid]['path'], array('title' => $menu['items'][$cid]['title']));
    $longdesc = $config['section'][$path]['items'][$menu['items'][$cid]['path']]['short-desc'];
    if ($longdesc) {
      $item .= " $longdesc";
    }
    $item .= '</li>';
    if ($counter++ % 2 == 0) {
      $col1 .= $item;
    }
    else {
      $col2 .= $item;
    }
  }
  $col1 .= '</ul></div>';
  $col2 .= '</ul></div>';
  $output .= $col1 . $col2;
  $output .= '</div>';
  $output .= '</div>';
  return $output;
}

function theme_cs_administration_sub_menu_panel() {
  drupal_set_title('');
  $config = _administration_menu_config();
  $menu = menu_get_menu();
  $mid = menu_get_active_item();
  $path = $menu['items'][$mid]['path'];

  $output .= '<div class=dashboard-panel>';
  $output .= '<div class=dashboard-panel-head>';
  $output .= $menu['items'][$mid]['description'];
  $output .= '</div>';
  $output .= '<div class=dashboard-panel-body>';

  $output .= '<div class=dashboard-panel-desc>';
  $output .= $config['section'][$path]['long-desc']; 
  $output .= '</div>';

  $output .= '<ul>';
  foreach ($menu['visible'][$mid]['children'] as $cid) {
    $output .= '<li>';
    $title = !empty($menu['items'][$cid]['description']) ? $menu['items'][$cid]['description'] : $menu['items'][$cid]['title'];
    $output .= l($title, $menu['items'][$cid]['path'], array('title' => $menu['items'][$cid]['title']));
    $longdesc = $config['section'][$path]['items'][$menu['items'][$cid]['path']]['long-desc'];
    if ($longdesc) {
      $output .= " $longdesc";
    }
    $output .= '</li>';
  }
  $output .= '</ul>';
  $output .= '</div>';
  $output .= '</div>';
  return $output;
}

function theme_administration_statistics_panel($stats, $path = NULL) {
  $config = _administration_menu_config();
  $menu = menu_get_menu();
  if (!$path) {
    $mid = menu_get_active_item();
    $path = $menu['items'][$mid]['path'];
  }
  else {
    $mid = $menu['path index'][$path];
  }

  $output .= '<div class=dashboard-panel>';
  $output .= '<div class=dashboard-panel-head>';
  $output .= $menu['items'][$mid]['description'];
  $output .= '</div>';
  $output .= '<div class=dashboard-panel-body>';

  $output .= '<div class=dashboard-panel-desc>';
  $output .= $config['section'][$path]['long-desc']; 
  $output .= '</div>';

  $output .= '<table>';
  $counter = 0;
  $last = count($stats);
  foreach ($stats as $stat) {
    $item .= "<th>$stat[title]</th><td>$stat[value]</td>";
    if ($counter++ % 2 == 1 || $counter == $last) {
      $output .= "<tr>$item</tr>\n";
      $item = "";
    }
  }
  $output .= '</table>';
  $output .= '</div>';
  $output .= '</div>';
  return $output;

}
/**
 * This function generates some basic site statistics, then sends them 
 * into a theme function for display.
 */
function theme_administration_statistics($path) {
  $users = db_result(db_query("SELECT COUNT(*) FROM users WHERE status = 1"));
  $stats['users'] = array('title' => "Users", 'value' => $users);

  $nodes = db_result(db_query("SELECT COUNT(*) FROM node WHERE status = 1"));
  $stats['nodes'] = array('title' => "Published Nodes", 'value' => $nodes);

  $comments = db_result(db_query("SELECT COUNT(*) FROM comments WHERE status = 1"));
  $stats['comments'] = array('title' => "Published comments", 'value' => $comments);

  $config = administration_menu_config();
  return theme('administration_statistics_panel', $stats, $path);
}

?>
