<?php

/*
 * The administration menu config has three basic pieces. 'section' which
 * are separate because they need their own menu entries, 'menu'
 * which need to be individually reconfigured, and 'alias' items in
 * case we want to alias menu items around.
 *
 * Each section entry has:
 * 'title' => The short title
 * 'description' => The longer descriptive title
 * 'callback' => The callback to use for this section. If blank no menu entry
 *               will be provided.
 * 
 * Each menu entry has
 *  'title' => The short title of the menu entry
 *  'description' => The longer desc
 *  'weight' => A weight can be specified; this will override the default weight 
 *              and is not recommended; the default weight will put menu items
 *              in the order they appear in the array.
 */
function _administration_menu_config() {
  $config['cssfile'] = 'merged.css';
  $config['top_path'] = 'administer';
  $config['section']['administer/config'] = array(
    'title'       => t('site'),
    'description' => t('Site and feature settings'),
    'callback'    => '_administration_sub_dashboard_page',
    'panel_theme' => 'administration_menu_panel',
    'sub_panel_theme' => 'administration_sub_menu_panel',
    'long-desc'   => t('You can configure new applications, known as modules and learn how to add new features.'),
    'panel_collapsible' => TRUE,
    'menu_expandable' => TRUE,
    'menu_as_icons' => FALSE,
    'items' => array(
      'admin/modules' => array(
        'title' => 'module control',
        'description' => 'Activate/deactivate modules',
      ),
      'admininister/config/browse' => array(
        'title' => 'browse modules',
        'description' => 'Browse available modules',
        'short-desc' => 'Browse a complete list of modules available for your site',
        'callback' => 'administration_offsite',
        'callback arguments' => array('http://drupal.org/project/Modules/4.7'),
      ),
      'administer/config/update' => array(  //NEED TO CHANGE THIS TO USE base_path()
        'title' => 'update site',
        'description' => "Run update scripts",
        'short-desc' => t('Run update scripts after site update files have been installed'),
        'callback' => 'administration_offsite',
        'callback arguments' => array('update.php'),
      ),
       'admin/settings' => array(
        'title' => 'module configuration',
        'description' => "Configure my site's module settings",
      ),
      'admin/filters' => array(
        'title' => 'input formats',
        'description' => "Configure allowable input formats/input filters",
      ),
      'admin/contact' => array(
        'title' => 'contact forms',
        'description' => "Configure contact forms for the site",
      ),
      'admin/locale' => array(
        'title' => 'localization',
        'description' => "Configure my site's language options",
      ),
      'admin/logs' => array(
        'title' => 'logs',
        'description' => "View my site's logs",
      ),
      'admin' => array(
        'title' => 'uncategorized',
        'description' => "Configure uncategorized items for modules",
        'short-desc' => "Uncategorized items may be recategorized in menu system"
      ),
    ),
  );

  $config['section']['administer/content'] = array(
    'title'       => t('content'),
    'description' => t('Content management'),
    'callback'    => '_administration_sub_dashboard_page',
    'panel_theme' => 'administration_menu_panel',
    'sub_panel_theme' => 'administration_sub_menu_panel',
    'long-desc'   => t('Web pages are treated as content that can be managed with settings.'),
    'panel_collapsible' => TRUE,
    'menu_expandable' => TRUE,
    'menu_as_icons' => FALSE,
    'items' => array(
      'admin/node' => array(
        'title' => 'manage content',
        'description' => "Moderate, promote and manage my web pages",
      ),
      'admin/comment' => array(
        'title' => 'manage comments',
        'description' => "Moderate and publish comments",
      ),
      'admin/taxonomy' => array(
        'title' => 'manage categories',
        'description' => "Set up and maintain my site's categories (vocabularies)",
      ),
      'admin/node/types' => array(
        'title' => 'manage content types',
        'description' => "Create and manage content types",
      ),      
      'admin/settings/content-types' => array(
        'title' => 'configure content types',
        'description' => "Configure content types",
      ),      
      'node/add' => array(
        'title' => 'create content',
        'description' => "Add new web pages (content) to my site",
      ),
      'admin/path' => array(
        'title' => 'url aliases',
        'description' => "Create and manage URL aliases",
      ),
      'admin/forum' => array(
        'title' => 'forums',
        'description' => "Configure my site's forums",
      ),
      'admin/settings/node' => array(
        'title' => 'post options',
        'description' => "Configure my site's posting options",
      ),
    )
  ); 

  $config['section']['administer/users'] = array(
    'title'       => t('config'),
    'description' => t('Users and groups'),
    'callback'    => '_administration_sub_dashboard_page',
    'panel_theme' => 'administration_menu_panel',
    'sub_panel_theme' => 'administration_sub_menu_panel',
    'long-desc'   => t('Administering users, groups and access control.'),
    'panel_collapsible' => TRUE,
    'menu_expandable' => TRUE,
    'menu_as_icons' => FALSE,
    'items' => array(
      'admin/user' => array(
        'title' => 'manage users',
        'description' => "Change or add users",
      ),
      'admin/settings/profile' => array(
        'title' => 'manage profiles',
        'description' => "Change or add user profile information",
      ),
      'admin/access' => array(
        'title' => 'access control',
        'description' => t("Manage user access to site features"),
      ),
    ),
  );

  $config['section']['administer/help'] = array(
    'title'       => t('help'),
    'description' => t('Help and documentation'),
    'callback'    => '_administration_sub_dashboard_page',
    'panel_theme' => 'administration_menu_panel',
    'sub_panel_theme' => 'administration_sub_menu_panel',
    'long-desc'   => t('Helpful links to documentation and other support information.'),
    'panel_collapsible' => TRUE,
    'menu_expandable' => TRUE,
    'menu_as_icons' => FALSE,
    'items' => array(
      'admin/help' => array(
        'title' => 'help',
        'description' => 'Module help',
      ),
      'administer/help/support' => array(
        'title' => 'get support',
        'description' => 'Get support',
        'short-desc' => 'A page that explains support options, including paid support',
        'long-desc' => 'This exists as an example of what the long-desc will do',
        'callback' => 'administration_offsite',
        'callback arguments' => array('http://drupal.org/support'),
      ),
      'administer/help/terminology' => array(
        'title' => 'terminology',
        'description' => 'Understanding terminology and concepts',
        'short-desc' => 'An introduction to Drupal terminology',
        'long-desc' => 'This exists as an example of what the long-desc will do',
        'callback' => 'administration_offsite',
        'callback arguments' => array('http://drupal.org/node/21951'),
      ),
      'administer/help/chat' => array(
        'title' => 'chat',
        'description' => 'Chat live to volunteers',
        'short-desc' => 'Well researched questions may be answered',
        'callback' => 'administration_offsite',
        'callback arguments' => array('http://drupalirc.com'),
      ),
      'administer/help/handbook' => array(
        'title' => 'handbooks',
        'description' => 'Drupal.org handbooks',
        'short-desc' => 'Drupal online documention',
        'callback' => 'administration_offsite',
        'callback arguments' => array('http://drupal.org/handbook'),
      ),
    ),
  );

  // This special one clones the system settings section so it goes into the
  // right place
  $config['section']['administer/config/settings'] = array(
    'title'       => t('site settings'),
    'description' => t('Configure site-wide settings'),
    'weight'      => -99,
    'callback'    => 'system_site_settings',
  );

  $config['section']['administer/design'] = array(
    'title'       => t('site design'),
    'description' => t('Style and layout'),
    'callback'    => '_administration_sub_dashboard_page',
    'panel_theme' => 'administration_menu_panel',
    'sub_panel_theme' => 'administration_sub_menu_panel',
    'long-desc'   => t('Alter your site\'s style and layout.'),
    'panel_collapsible' => TRUE,
    'menu_expandable' => TRUE,
    'menu_as_icons' => FALSE,
    'items' => array(
      'admin/themes' => array(
        'title' => 'themes',
        'description' => "Activate/configure themes",
      ),
      'admin/block' => array(
        'title' => 'blocks',
        'description' => "Activate/configure blocks",
      ),
     'admin/menu' => array(
        'title' => 'menus',
        'description' => "Manage my site's menus",
      ),
     'admin/settings/menu' => array(
        'title' => 'menus',
        'description' => "Configure menu settings",
      ),
      'administer/design/browse' => array(
        'title' => 'browse',
        'description' => 'Browse themes',
        'callback' => 'administration_offsite',
        'callback arguments' => array('http://drupal.org/project/Themes/4.7'),
      ),
      'administer/design/problems' => array(
        'title' => 'fix themes',
        'description' => 'Advice on theme problems',
        'callback' => 'administration_offsite',
        'callback arguments' => array('http://drupal.org/node/37156'),
      ),
    ),
  );

  $config['section']['administer/mail'] = array(
    'title'       => t('mail'),
    'description' => t('Mail and subscriptions'),
    'callback'    => '_administration_sub_dashboard_page',
    'panel_theme' => 'administration_menu_panel',
    'sub_panel_theme' => 'administration_sub_menu_panel',
    'long-desc'   => t('Control your site\'s mail and subscriptions.'),
    'panel_collapsible' => TRUE,
    'menu_expandable' => TRUE,
    'menu_as_icons' => FALSE,
    'items' => array(
      'admin/aggregator' => array(
        'title' => 'news feeds',
        'description' => "Manage my news feeds",
      ),
      'admin/settings/aggregator' => array(
        'title' => 'configure news feeds',
        'description' => "Configure news feed settings",
      ),
    ),
  );

  $config['section']['administer/site'] = array(
    'title'       => t('site statistics'),
    'description' => t('Site Statistics and Logs'),
    'callback'    => '_administration_sub_dashboard_page',
    'panel_theme' => 'administration_tabbed_panel',
    'sub_panel_theme' => 'administration_sub_menu_panel',
    'long-desc'   => t('View my site\'s statistics and error logs.'),
    'panel_collapsible' => TRUE,
    'menu_expandable' => TRUE,
  );

  $config['section']['administer/site/general_stats'] = array(
    'title'       => t('general'),
    'description' => t('General'),
    'callback'    => '_administration_site_statistics',
    'callback arguments' => array('general'),
    'panel_theme' => 'administration_tabbed_panel',
    'weight'      => -99,
    'long-desc'   => t('View general site statistics.')
  );
  $config['section']['administer/site/content_stats'] = array(
    'title'       => t('popular content'),
    'description' => t('Popular Content'),
    'callback'    => '_administration_site_statistics',
    'callback arguments' => array('content'),
    'panel_theme' => 'administration_tabbed_panel',
    'weight'      => -98,
    'long-desc'   => t('View most popular site content.')
  );
  $config['section']['administer/site/logs'] = array(
    'title' => t('view logs'),
    'description' => t('View system logs'),
    'callback'    => 'administration_watchdog_overview',
    'panel_theme' => 'administration_tabbed_panel',
    'weight'      => -97,
  );


  // overrides to repoint a couple of settings.
  $config['section']['admin/settings'] = array(
    'title' => t('configure modules'),
    'description' => t("Configure my site's module settings"),
    'callback'    => '_administration_settings_page',
  );
  
  // overrides to repoint a couple of settings.
  $config['section']['admin/uncategorized'] = array(
    'title' => t('configure modules'),
    'description' => t("Configure uncategorized module settings"),
    'callback'    => '_administration_sub_dashboard_page',
  );

  $config['dashboard']['dashboard-container1'] = array(
    'administer/config',
    'administer/content',
    'administer/design',
    'administer/mail',
  );

  $config['dashboard']['dashboard-container2'] = array(
    'administer/users',
    'administer/site',
    'administer/help',
  );

  return $config;
}
function theme_administration_menu_panel($path, $container, $num, $count) {
  $config = administration_menu_config();
  _administration_set_js_head();

  $menu = menu_get_menu();
  $mid = $menu['path index'][$path];
  if ($config['section'][$path]['panel_collapsible']) $collapsible = 'collapsible';
  $output .= '<div class="dashboard-panel ' . $collapsible .'">';
  $output .= '<div class="dashboard-panel-head head">';
  $output .= $menu['items'][$mid]['description'];

  $which = substr($container, -1, 1);
  if ($which == 1) {
    $icon = "[&rarr;]";
  }
  else {
    $icon = "[&larr;]";
  }
  $output .= '<span class="controls">';
  $output .= l($icon, "administer/panelshift/$which/$num/swap", NULL, NULL, NULL, FALSE, TRUE);

  if ($num != 0) {
    $output .= l("[&uarr;]", "administer/panelshift/$which/$num/up", NULL, NULL, NULL, FALSE, TRUE);
  }
  if ($num != $count - 1) {
    $output .= l("[&darr;]", "administer/panelshift/$which/$num/down", NULL, NULL, NULL, FALSE, TRUE);
  }
  $output .= '</span>';
  $output .= '</div>';
  $output .= '<div name="collapsible-panel" class="dashboard-panel-body body">';
  $output .= '<div class="dashboard-panel-desc">';
  $output .= $config['section'][$path]['long-desc']; 

  $output .= '</div>';

  if ($config['section'][$path]['menu_as_icons'] && module_exist('controlpanel')) {
    $output .= controlpanel_build_controlpanel($mid, FALSE);
  }
  else {
    if ($config['section'][$path]['menu_expandable']) $collapsible = 'collapsible';
    $output .= '<ul class="' . $collapsible . '">';
    foreach ((array) $menu['visible'][$mid]['children'] as $cid) {
      $title = !empty($menu['items'][$cid]['description']) ? $menu['items'][$cid]['description'] : $menu['items'][$cid]['title'];
      $src = _administration_get_icon_file_name($menu['items'][$cid]['path']);
      if (!empty($menu['visible'][$cid]['children'])) {
        $output .= '<li class="item-list" style="background-image: url('. $src . ');">';
        $output .= l($title, $menu['items'][$cid]['path'], array('title' => $menu['items'][$cid]['title']));
        $output .= '<ul class="collapsed">';
        foreach ($menu['visible'][$cid]['children'] as $ccid) {
          $title = !empty($menu['items'][$ccid]['description']) ? $menu['items'][$ccid]['description'] : $menu['items'][$ccid]['title'];
          $output .= '<li class="item-list" style="background-image: url('. $src . ');">';
          $output .= l($title, $menu['items'][$ccid]['path'], array('title' => $menu['items'][$ccid]['title']));
          $output .= '</li>';
        }
        $output .= '</ul>';
        $output .= '</li>';
      }
      else {     
        $output .= '<li class="item-list" style="background-image: url('. $src . ');">';
        $output .= l($title, $menu['items'][$cid]['path'], array('title' => $menu['items'][$cid]['title']));
        $output .= '</li>';
      }
    }
  }

  $output .= '</ul>';
  $output .= '</div>';
  $output .= '</div>';

  return $output;
}

function theme_administration_sub_menu_panel() {
  $menu = menu_get_menu();
  $mid = menu_get_active_item();
  $output .= '<div class="dashboard-panel">';
  $output .= '<div class="dashboard-panel-head">';
  $output .= $menu['items'][$mid]['description'];
  $output .= '</div>';
  $output .= '<div class="dashboard-panel-body">';
  $output .= administration_help($menu['visible'][$mid]['path']);
  $output .= '<ul>';
  foreach ($menu['visible'][$mid]['children'] as $cid) {
    $src = _administration_get_icon_file_name($menu['items'][$cid]['path']);
    $output .= '<li class="item-list" style="background-image: url('. $src . ');">';
    $title = !empty($menu['items'][$cid]['description']) ? $menu['items'][$cid]['description'] : $menu['items'][$cid]['title'];
    $output .= l($title, $menu['items'][$cid]['path'], array('title' => $menu['items'][$cid]['title']));
    $output .= '</li>';
  }
  $output .= '</ul>';
  $output .= '</div>';
  $output .= '</div>';
  return $output;
}

function theme_administration_tabbed_panel($path){
  $config = administration_menu_config();
  _administration_set_js_head();
  $menu = menu_get_menu();
  $mid = $menu['path index'][$path];
  if ($config['section'][$path]['panel_collapsible']) $collapsible = 'collapsible';
  $output .= '<div class="dashboard-panel ' . $collapsible .'">';
  $output .= '<div class="dashboard-panel-head head">';
  $output .= $menu['items'][$mid]['description'];
  $which = substr($container, -1, 1);
  if ($which == 1) {
    $icon = "[&rarr;]";
  }
  else {
    $icon = "[&larr;]";
  }
  $output .= '<span class="controls">';
  $output .= l($icon, "administer/panelshift/$which/$num/swap", NULL, NULL, NULL, FALSE, TRUE);

  if ($num != 0) {
    $output .= l("[&uarr;]", "administer/panelshift/$which/$num/up", NULL, NULL, NULL, FALSE, TRUE);
  }
  if ($num != $count - 1) {
    $output .= l("[&darr;]", "administer/panelshift/$which/$num/down", NULL, NULL, NULL, FALSE, TRUE);
  }
  $output .= '</div>';
  $output .= '<div class="dashboard-panel-body body">';
  $output .= '<div class=dashboard-panel-desc>';
  $output .= $config['section'][$path]['long-desc']; 
  $output .= '</div>';

  $output.= '<div class=tabs>';
  $output.= '<ul class="tabs primary">';

  $param = $_GET['panel_' . $path];
  if ($param) variable_set('administration_stats_default_tab', $param);
  if (variable_get('administration_stats_default_tab', NULL) != NULL & !$param) {
    $param = variable_get('administration_stats_default_tab', NULL);
  }

  $i=0;
  foreach ($menu['visible'][$mid]['children'] as $cid) {
    if ($i==0) $default_tab = $menu['visible'][$cid]['path'];
    $active = (($menu['items'][$cid]['path']==$param) | (!$param && $i==0)) ? 'active' : '';
    $attributes = array('class' => $active);
    $output .= '<li class=' . $active . '>';
    $title = $menu['items'][$cid]['title'];
    $output .= _administration_build_tabbed_link($menu['visible'][$cid]['title'], $attributes, $path, $menu['items'][$cid]['path']);
    $output .= '</li>';
    $i++;
  }
  $output .= '</ul>';
  $output .= '</div>';
  if (!$param) $param = variable_get('administration_stats_default_tab', $default_tab); 

  $return = _administration_execute_active_handler($param);
  
  $output .= $return;
  $output .= '</div>';
  $output .= '</div>';

  return $output;
}

function _administration_build_tabbed_link($text, $attributes, $panel_id, $tab_id){
  $query = 'panel_' . $panel_id . '=' . $tab_id;

  foreach ($_GET as $key => $value) {
    if (($key != 'q') && ($key != 'panel_' . $panel_id)) {
      $query .= '&' . $key . '=' . $value;
    }
  }

  return '<a href="'. check_url(url($_GET['q'], $query, NULL, NULL)) .'"'. 
    drupal_attributes($attributes) .'>'. 
    ($html ? $text : check_plain($text)) .'</a>';  
}

function _administration_execute_active_handler($menu_path) {
  $menu = menu_get_menu();
  $mid = $menu['path index'][$menu_path];
  if (empty($menu['callbacks'][$menu_path]['callback'])) {
    return MENU_NOT_FOUND;
  }
  $arguments = array_key_exists('callback arguments', $menu['callbacks'][$menu_path]) ? $menu['callbacks'][$menu_path]['callback arguments'] : array();
  $arg = substr($_GET['q'], strlen($menu['items'][$mid]['menu_path']) + 1);
  if (strlen($arg)) {
    $arguments = array_merge($arguments, explode('/', $arg));
  }
  return function_exists($menu['callbacks'][$menu_path]['callback']) ?  call_user_func_array($menu['callbacks'][$menu_path]['callback'], $arguments) : '';
}

function _administration_site_statistics($op) {
  $report_config['last_week'] = strtotime("-1 week");
  $report_config['last_month'] = strtotime("-1 month");
  $report_config['last_year'] = strtotime("-1 year");
  $report_config['all_time'] = 0;
  $report_config['intervals'] = array(
    t('week') => $report_config['last_week'], 
//    t('month') => $report_config['last_month'], 
//    t('year') => $report_config['last_year'], 
    t('epoch') => 0
  );

  switch ($op) {
    case 'general':
      $sql="SELECT MIN(created) FROM {node} WHERE status=1 AND moderate=0";
      $first_tstmp = db_result(db_query($sql));
      $founded = format_date($first_tstmp, "long");
      $report[t("This site's first publication was ")] = $founded; 

      $duration = format_interval(time() - $first_tstmp, 3);
      $report[t("We have been online for ")] = $duration; 

      $sql="SELECT COUNT(*) FROM {users} WHERE status=1";
      $count = db_result(db_query($sql));
      $report[t("Active registered user accounts: ")] = $count; 

      $items = array();
      $intervals = $report_config['intervals'];
      reset($intervals);
      while (list($interval, $tstmp) = each($intervals)) {
        $sql = "SELECT count(*) FROM {node} where promote=1 and status=1 and moderate=0 and created>=" . $intervals[$interval];
        $count = db_result(db_query($sql));
        if ($interval == t('epoch')) {
          $report[("Promoted Nodes published since this site was founded: ")] = $count; 
        } 
        else {
          $report[t("Promoted Nodes published in the last ") . $interval . ": " ] = $count;
        }
      }

      reset($intervals);
      while (list($interval, $tstmp) = each($intervals)) {
        $sql = "SELECT count(*) FROM {node} where status=1 and moderate=0 and created>=" . $intervals[$interval];
        $count = db_result(db_query($sql));
        if ($interval == t('epoch')) {
          $report[t("Total Nodes published since this site was founded: ")] = $count;
        } else {
          $report[t("Total Nodes published in the last ") . $interval . ": " ] = $count;
        }
      } 
      if (module_exist('scheduler')) {
        $sql="SELECT COUNT(*) FROM {node} n LEFT JOIN {scheduler} s ON n.nid=s.nid WHERE (n.status=0 OR n.moderate=1) AND s.nid IS NULL";
      } else {
        $sql="SELECT COUNT(*) FROM {node} n WHERE n.status=0 OR n.moderate=1";
      }
      $count = db_result(db_query($sql));
      $report[t("Items in editorial queue, awaiting approval: ")] = $count;
      if (module_exist('scheduler')) {
        $sql="select count(*) FROM {scheduler}";
        $count = db_result(db_query($sql));
        $report[t("Items approved for automatic publication in the future: ")] = $count;
      }

      return theme('administration_statistics_list',$report);
      
    case 'content':
      $popular_limit = variable_get('administration_pop_item_count',5);
      $edit = $_POST['edit'];
      if (!isset($edit) | $edit['report_interval'] == 'all_time') {
        $interval = $report_config[variable_get('administration_content_interval', 'all_time')];
      }
      else {
        $interval = $report_config[$edit['report_interval']];
        variable_set('administration_content_interval', $edit['report_interval']);
      }

      $pops = array();
      $sql = "select n.nid,n.title,c.totalcount from {node} n left join {node_counter} c on n.nid=c.nid where n.status=1 and n.created>=" . $interval . " and c.totalcount>1 and title not like '%page not found%' order by c.totalcount desc, n.created desc limit " . $popular_limit;
      $result = db_query($sql);
      while ($row = db_fetch_array($result)) {
        $pops[] = $row;
      }
      foreach($pops as $list) {
        $rows[] = array(l($list['title'], 'node/' . $list['nid']), $list['totalcount'] . "&nbsp;" . t("reads"));
      }
      $options = array(
        'last_week' => t('week'), 
        'last_month' =>t('month'), 
        'last_year' => t('year'), 
        'all_time' => t('all time')
      );

      $form['report_interval'] = array(
        '#type' => 'select', 
        '#default_value' => variable_get('administration_content_interval', 'all_time'),
        '#options' => $options 
      );
      $form['submit'] = array('#type' => 'submit', 
      '#value' => t('Show')
      );
      $output .= drupal_get_form('administration_report_form', $form);
      
      $head = array('item','hits');      
      $attributes = array('width'=>'100%');
      $output .= theme('table', $head, $rows, $attributes);
      return $output;
  }
}

function theme_administration_statistics_list($report){
  $output .= '<ul>';
  foreach ($report as $key => $value) {
    $output .= '<li class=stats-list>';
    $output .= $key . $value;
    $output .= '</li>';
  }
  $output .= '</ul>';
  return $output;
}

function administration_form_merged($form){
  $form['merged'] = array(
    '#type' => 'fieldset', '#title' => t('Settings for default dashboard theme'),
    '#collapsible' => FALSE, '#collapsed' => FALSE
  );
  $form['merged']['menu_icons'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use graphical icons for menu bullets within the administration dashboard'),
    '#default_value' => variable_get('administration_show_menu_icons', FALSE),
    '#tree' => FALSE
  );
  return $form;
}

function administration_form_submit_merged($formid, $form_values) {
  variable_set('administration_show_menu_icons', $form_values['menu_icons']);
}

function _administration_get_icon_file_name($path) {
  if (variable_get('administration_show_menu_icons', FALSE) == TRUE) {
    $image_directory = drupal_get_path('module', 'administration') . '/images/' . variable_get('administration_list_icon_size', '14x14');
    $working_path = $path;
    if (is_numeric(substr($working_path, strrpos($working_path, "/") + 1))) {
      $working_path = substr($working_path, 0, strrpos($working_path, "/"));
    }  
    $new_path = str_replace(':', '', $working_path); 
    $file_name = $image_directory . '/' . str_replace('/', '_', $new_path) . '.png'; 
    if (file_exists($file_name)) {
      $src = $file_name;
    } 
    else {
      $src = $image_directory . '/default.png';
    }
  }
  else {
    $src = '/' . drupal_get_path('module', 'administration') . '/images/bullet.png';
  }
  return $src;
}

function _administration_set_js_head() {
  $config = administration_menu_config();
  $cssfile = $config['cssfile'] ? $config['cssfile'] : 'default.css';
  static $sent = array(); 
  if (!isset($sent[drupal_get_path('module','administration') . '/collapse_list.js'])) {
    $module_path = drupal_get_path('module','administration');
    drupal_add_js($module_path . '/collapse_list.js');
    $sent[drupal_get_path('module','administration') . '/collapse_list.js'] = TRUE;
  }  
  if (!isset($sent[drupal_get_path('module','administration') . '/collapse_div.js'])) {
    $module_path = drupal_get_path('module','administration');
    drupal_add_js($module_path . '/collapse_div.js');
    $sent[drupal_get_path('module','administration') . '/collapse_div.js'] = TRUE;
  }  

  return;
}

/*
 * Modified version of 'watchdog_overview' function
 * TODO - submit patches to the watchdog module to allow greater 
 * flexibility when displaying log records
 */
function administration_watchdog_overview() {
  $page_size = 6;
  $message_size = 30;
  $date_format = 'm/d H:m';
  
  $icons = array(WATCHDOG_NOTICE  => '',
                 WATCHDOG_WARNING => theme('image', 'misc/watchdog-warning.png', t('warning'), t('warning')),
                 WATCHDOG_ERROR   => theme('image', 'misc/watchdog-error.png', t('error'), t('error')));
  $classes = array(WATCHDOG_NOTICE => 'watchdog-notice', WATCHDOG_WARNING => 'watchdog-warning', WATCHDOG_ERROR => 'watchdog-error');

  $names['all'] = t('all messages');
  
  $types = array();
  $result = db_query('SELECT DISTINCT(type) FROM {watchdog} ORDER BY type');
  while ($object = db_fetch_object($result)) {
    $types[] = $object->type;
  }  
  foreach ($types as $type) {
    $names[$type] = t('%type messages', array('%type' => t($type)));
  }
  if (empty($_SESSION['watchdog_overview_filter'])) {
    $_SESSION['watchdog_overview_filter'] = 'all';
  }

  $form['filter'] = array(
    '#type' => 'select',
    '#title' => t('Filter by message type'),
    '#options' => $names,
    '#default_value' => $_SESSION['watchdog_overview_filter']
  );
  //$form['#action'] = url('admin/logs');

  $form['submit'] = array('#type' => 'submit', '#value' =>t('Filter'));
  $form['view'] = array('#type' => 'submit', '#value' =>t('View full page'));
  $output = drupal_get_form('administration_watchdog_form_overview', $form);

  $header = array(
    ' ',
    array('data' => t('Type'), 'field' => 'w.type'),
    array('data' => t('Date'), 'field' => 'w.wid', 'sort' => 'desc'),
    array('data' => t('Message'), 'field' => 'w.message'),
    array('data' => t('User'), 'field' => 'u.name'),
//    array('data' => t('Operations'))
  );

  $sql = "SELECT w.*, u.name, u.uid FROM {watchdog} w INNER JOIN {users} u ON w.uid = u.uid";
  $tablesort = tablesort_sql($header);
  $type = $_SESSION['watchdog_overview_filter'];
  if ($type != 'all') {
    $result = pager_query($sql ." WHERE w.type = '%s'". $tablesort, $page_size, 0, NULL, $type);
  }
  else {
    $result = pager_query($sql . $tablesort, $page_size);
  }

  while ($watchdog = db_fetch_object($result)) {
    $rows[] = array('data' =>
      array(
        // Cells
        $icons[$watchdog->severity],
        t($watchdog->type),
        //format_date($watchdog->timestamp, 'small'),
        format_date($watchdog->timestamp, 'custom', $date_format),
        l(truncate_utf8($watchdog->message, $message_size, TRUE, TRUE), 'admin/logs/event/'. $watchdog->wid, array(), NULL, NULL, FALSE, TRUE),
        theme('username', $watchdog),
//        $watchdog->link,
      ),
      // Attributes for tr
      'class' => "watchdog-". preg_replace('/[^a-z]/i', '-', $watchdog->type) .' '. $classes[$watchdog->severity]
    );
  }

  if (!$rows) {
    $rows[] = array(array('data' => t('No log messages available.'), 'colspan' => 6));
  }

  $output .= '<div class=panel-logs>';
  $output .= theme('table', $header, $rows);
  $output .= '</div>';
  $output .= theme('pager', NULL, $page_size, 0);

  return $output;
}

/*
 * Modified version of 'theme_watchdog_form_overview' function
 * TODO - submit patches to the watchdog module to allow greater 
 * flexibility when displaying log records
 */
function theme_administration_watchdog_form_overview($form) {
  return '<div class="container-inline">'. form_render($form) .'</div>';
}

/*
 * Modified version of 'watchdog_form_overview_submit' function
 * TODO - submit patches to the watchdog module to allow greater 
 * flexibility when displaying log records
 */
function administration_watchdog_form_overview_submit($form_id, $form) {
  global $form_values;
  $op = isset($_POST['op']) ? $_POST['op'] : '';
  $_SESSION['watchdog_overview_filter'] = $form_values['filter'];
  if ($op == 'View full page') {
    drupal_goto('admin/logs');
  }
}

?>
